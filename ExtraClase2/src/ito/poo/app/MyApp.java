package ito.poo.app;
import ito.poo.clases.act2.*;
import java.time.LocalDate;
public class MyApp {
	
	static void run() {
		// pacientes
		LocalDate fechanac= LocalDate.of(2001, 12, 13);
		pacientes pc = new pacientes("Derick Shelby Wyne", "DSW978", fechanac, 1, 19);
		System.out.println(pc);
		
		//consultas
		LocalDate fechanaci= LocalDate.of(2001, 12, 13);
		consultas cs = new consultas(fechanaci, "dolor de cabeza,nauseas,antojos", 38, 50, 165);
		System.out.println(cs);
		
		//diagnostico
		
		diagnostico dg= new diagnostico("esclerosis lateral amiotrofica", "paracetamol", "no tomar clases virtuales \nestresa mucho y causa da�os psicologicos");
		System.out.println(dg);
	
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
